# Build stage
FROM maven:3.6-alpine AS build

WORKDIR /app/

COPY ./pom.xml .
COPY ./src .

RUN mvn clean package -Dmaven.test.skip=true

# Final stage
FROM openjdk:8-jdk-alpine

COPY --from=build /app/target/*.jar app.jar

ENTRYPOINT ["java","-jar","/app.jar"]