package de.max.springmongo.domain.adress;

import static org.apache.commons.lang3.Validate.notNull;

public class City {

  private String city;

  public City(final String city) {
    notNull(city,"city must not be null");
    this.city = city;
  }

  public String getCity() {
    return city;
  }

  public void setCity(final String city) {
    notNull(city,"city must not be null");
    this.city = city;
  }
}
