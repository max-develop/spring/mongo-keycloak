package de.max.springmongo.domain.adress;

import static org.apache.commons.lang3.Validate.notNull;

public class Street {

  private String street;

  public Street(final String street) {
    notNull(street,"street must not be null");
    this.street = street;
  }

  public String getStreet() {
    return street;
  }

  public void setStreet(final String street) {
    notNull(street,"street must not be null");
    this.street = street;
  }
}
