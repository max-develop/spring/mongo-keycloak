package de.max.springmongo.domain.adress;

import static org.apache.commons.lang3.Validate.notNull;

public class Country {

  private String country;

  public Country(final String country) {
    notNull(country,"country must not be null");
    this.country = country;
  }

  public String getCountry() {
    return country;
  }

  public void setCountry(final String country) {
    notNull(country,"country must not be null");
    this.country = country;
  }
}
