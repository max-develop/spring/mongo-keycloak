package de.max.springmongo.domain.adress;

import static org.apache.commons.lang3.Validate.notNull;

public class ZIP {

  private Integer zip;

  public ZIP(Integer zip) {
    notNull(zip,"zip must not be null");
    this.zip = zip;
  }

  public Integer getZip() {
    return zip;
  }

  public void setZip(final Integer zip) {
    notNull(zip,"zip must not be null");
    this.zip = zip;
  }
}
