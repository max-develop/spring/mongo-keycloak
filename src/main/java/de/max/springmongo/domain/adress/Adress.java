package de.max.springmongo.domain.adress;

import static org.apache.commons.lang3.Validate.notNull;

public class Adress {

  private Street street;

  private ZIP zip;

  private Country country;

  public Adress(final Street street, final ZIP zip, final Country country) {
    notNull(street,"street must not be null");
    notNull(zip,"zip must not be null");
    notNull(country,"country must not be null");

    this.street = street;
    this.zip = zip;
    this.country = country;
  }

  public Street getStreet() {
    return street;
  }

  public void setStreet(final Street street) {
    notNull(street,"street must not be null");
    this.street = street;
  }

  public ZIP getZip() {
    return zip;
  }

  public void setZip(final ZIP zip) {
    notNull(zip,"zip must not be null");
    this.zip = zip;
  }

  public Country getCountry() {
    return country;
  }

  public void setCountry(final Country country) {
    notNull(country,"country must not be null");
    this.country = country;
  }

  @Override
  public String toString() {
    return "Adress{" + "street=" + street + ", zip=" + zip + ", country=" + country + '}';
  }
}
