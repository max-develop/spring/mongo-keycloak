package de.max.springmongo.domain.customer;

import static org.apache.commons.lang3.Validate.notNull;

public class FirstName {

  private String firstName;

  public FirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(final String firstName) {
    notNull(firstName,"firstName must not be null");
    this.firstName = firstName;
  }
}
