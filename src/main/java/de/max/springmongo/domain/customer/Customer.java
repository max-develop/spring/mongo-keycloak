package de.max.springmongo.domain.customer;

import org.springframework.data.annotation.Id;
import de.max.springmongo.domain.adress.Adress;

import static org.apache.commons.lang3.Validate.notNull;

public class Customer {

  @Id
  private String id;

  private FirstName firstName;

  private LastName lastName;

  private Adress address;

  public Customer(final String id,final FirstName firstName, final LastName lastName, final Adress address) {
    this.id = id;
    this.firstName = firstName;
    this.lastName = lastName;
    this.address = address;
  }

  public String getId(){
    return id;
  }

  public void setId(final String id){
    notNull(id,"id must not be null");
    this.id = id;
  }

  public FirstName getFirstName() {
    return firstName;
  }

  public void setFirstName(final FirstName firstName) {
    notNull(firstName,"firstName must not be null");
    this.firstName = firstName;
  }

  public LastName getLastName() {
    return lastName;
  }

  public void setLastName(final LastName lastName) {
    notNull(lastName,"lastName must not be null");
    this.lastName = lastName;
  }

  public Adress getAddress() {
    return address;
  }

  public void setAddress(final Adress address) {
    notNull(address,"address must not be null");
    this.address = address;
  }

  @Override
  public String toString() {
    return "Customer{" + "id='" + id + '\'' + ", firstName=" + firstName + ", lastName=" + lastName + ", address=" + address + '}';
  }
}
