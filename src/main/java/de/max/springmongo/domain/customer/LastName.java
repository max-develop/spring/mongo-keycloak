package de.max.springmongo.domain.customer;

import static org.apache.commons.lang3.Validate.notNull;

public class LastName {



  private String lastName;

  public LastName(String lastName) {
    this.lastName = lastName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(final String lastName) {
    notNull(lastName,"lastName must not be null");
    this.lastName = lastName;
  }
}
