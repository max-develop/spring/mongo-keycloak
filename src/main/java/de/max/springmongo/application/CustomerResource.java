package de.max.springmongo.application;

import de.max.springmongo.domain.customer.Customer;
import de.max.springmongo.domain.customer.CustomerRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("/api")
public class CustomerResource {

  private static final Logger LOG = LoggerFactory.getLogger(CustomerResource.class);

  @Autowired
  CustomerRepository repository;

  @RequestMapping(value = "/customers}", method = RequestMethod.GET)
  public @ResponseBody List<Customer> getCustomers(){
    LOG.info("Find all customers");
    List<Customer> customers = repository.findAll();

    return customers;
  }
}
